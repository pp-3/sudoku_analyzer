<template>
  <h2>Analyze a Sudoku game</h2>

  <div class="left">
    <div>
      <Menu v-bind:state="state"
            @new-game="newGame()"
            @predefined-finished="predefineFinished()" />
    </div>

    <div v-if="isFinished">
      <h2 class="highlight">Finished</h2>
    </div>

    <Field
      v-bind:state="state"
      v-bind:fields="fields"
      v-bind:show_hints="show_hints"
      @predefined-set="setPredefined($event.col, $event.row, $event.val)"
      @user-set="setUserValue($event.col, $event.row, $event.val)"
    />

    <div class="actions"
         v-if="isInAnalyze">
      <button v-on:click="undoLastStep()" v-bind:disabled="current_step < 0">Undo last move</button>
      <button v-on:click="redoNextStep()" v-bind:disabled="current_step + 1 >= steps.length">Redo next move</button>
      <button v-on:click="autoChooseNextStep()" v-bind:disabled="autoNextStep === undefined">Choose a move</button>
      <input type="checkbox" v-model="show_hints" name="hints" />
      <label for="hints">Show hints</label>
    </div>
  </div>

  <div v-if="isInAnalyze">
    <History
      v-bind:state="state"
      v-bind:steps="steps"
      v-bind:current_step="current_step"
      @history-pos-selected="gotoHistoryPos($event)"
      @history-pos-highlight="highlightHistoryPos($event)"
    />
  </div>
</template>

<script>
import Menu from './components/menu.vue'
import Field from './components/field.vue'
import History from './components/history.vue'
import Access from './components/access.js'
import States from './components/state.js'
import Step from './components/step.js'
import Hints from './components/hints.js'

/**
 * main application
 */
export default {
  name: 'App',
  components: {
    Menu,
    Field,
    History
  },
  data() {
    return {
      /**
       * current application state: init, setting fixed values, or analyze
       */
      state: States.INIT,
      /**
       * current history: list of Step.createHistoryStep() instances
       */
      steps: [],
      /**
       * index of current shown step
       */
      current_step: -1,
      /**
       * column array (9) of row arrays (9) with Cell instances
       */
      fields: [],
      /**
       * If true, the possible values for each cell are shown as a hint
       */
      show_hints: true
    }
  },
  computed: {
    /**
     * Returns true, if application is in analyzing state
     */
    isInAnalyze() {
      return this.state == States.ANALYZE
    },
    /**
     * Returns a Step object, if a legal move is possible.
     * @returns Step or undefined, if a move isn't determined
     */
    autoNextStep() {
      for (let col = 0; col < 9; ++col) {
        for (let row = 0; row < 9; ++row) {
          const cell = Access.field(this.fields, col, row)

          if (cell.unique_value !== undefined) {
            return Step.createStep(col, row, cell.unique_value)
          }
        }
      }

      return undefined
    },
    /**
     * Returns true, if all fields are set
     */
    isFinished() {
      if (this.fields.length < 9) {
        return false
      }

      for (let col = 0; col < 9; ++col) {
        for (let row = 0; row < 9; ++row) {
          if (this.getValue(col, row) === undefined) {
            return false
          }
        }
      }

      return true
    }
  },
  methods: {
    /**
     * Clears the playground and the history. Afterwards, a 9x9 field is accessible in the fields variable.
     * Access is via fields[column][row]. Column and row are in the range [0..9) and start in the top left corner.
     */
    clear() {
      this.fields = Access.newField()

      this.current_step = -1
      this.steps = []
    },
    /**
     * Starts a new game. Switches into the state for obtaining predefined values.
     */
    newGame() {
      this.state = States.PREDEFINED_INPUT
      this.clear()
    },
    /**
     * Switches into the analyzing state.
     */
    predefineFinished() {
      this.state = States.ANALYZE
      this.calcCellOptions()
    },
    /**
     * Sets a predefined value.
     */
    setPredefined(col, row, val) {
      let cell = Access.field(this.fields, col, row)
      cell.user_value = val
      cell.predefined = val !== undefined
    },
    /**
     * Sets a user added value into the internal field array.
     */
    setUserValue(col, row, val) {
      Access.field(this.fields, col, row).user_value = val
      this.addHistory(Step.createHistoryStep(col, row, val, this.user2list()))
      this.calcCellOptions()
    },
    /**
     * Calculates for each not yet set cell in the field array the list of possible (legal) values.
     * Afterwards unique values are searched for.
     */
    calcCellOptions() {
      let any_error = false

      for (let col = 0; col < 9; ++col) {
        for (let row = 0; row < 9; ++row) {
          let cell = Access.field(this.fields, col, row)
          cell.unique_value = undefined

          if (cell.predefined) {
            cell.current_error = !this.isValid(col, row)
            cell.valid_values.clear()
          } else if (cell.user_value !== undefined) {
            cell.current_error = !this.isValid(col, row)
            cell.valid_values.clear()

            if (cell.current_error) {
              any_error = true
            }
          } else {
            cell.current_error = false
            Hints.calculateValidValues(this.fields, col, row)
          }
        }
      }

      // in depth searches
      Hints.removeImpossibleCombinations(this.fields)

      if (!any_error) {
        for (let col = 0; col < 9; ++col) {
          for (let row = 0; row < 9; ++row) {
            Hints.calcUniqueValidValue(this.fields, row, col)
          }
        }
      }
    },
    /**
     * Returns true, if the user set value in the given row and column doesn't break a rule.
     * False is returned, if a rule violation is detected.
     */
    isValid(col, row) {
      return this.isValidInOwnSection(col, row) && this.isValidInCol(col, row) && this.isValidInRow(col, row)
    },
    /**
     * Checks the value in the given column and row for being unique in its own 3x3 section.
     * Returns true, if value is not yet set or is unique.
     *         false, if value collides with another cell in the same section.
     */
    isValidInOwnSection(col, row) {
      const val = this.getValue(col, row)

      if (val === undefined) {
        return true
      }

      const section_values = Access.getSectionValues(this.fields, col, row, col, row, Access.visibleValueExtractor)
      return !section_values.has(val)
    },
    /**
     * Checks the value in the given column and row for being unique in its column.
     * Returns true, if value is not yet set or is unique.
     *         false, if value collides with another cell in the same column.
     */
    isValidInCol(col, row) {
      const val = this.getValue(col, row)

      if (val === undefined) {
        return true
      }

      const col_values = Access.getColumnValues(this.fields, col, row, Access.visibleValueExtractor)
      return !col_values.has(val)
    },
    /**
     * Checks the value in the given column and row for being unique in its row.
     * Returns true, if value is not yet set or is unique.
     *         false, if value collides with another cell in the same row.
     */
    isValidInRow(col, row) {
      const val = this.getValue(col, row)

      if (val === undefined) {
        return true
      }

      const row_values = Access.getRowValues(this.fields, row, col, Access.visibleValueExtractor)
      return !row_values.has(val)
    },
    /**
     * Returns the effective value for the given column and row:
     * - if a predefined value is set, it is returned
     * - if the user has set a value, the user value is returned
     * - otherwise, undefined is returned
     */
    getValue(col, row) {
      return Access.visibleValueExtractor(Access.field(this.fields, col, row))
    },
    /**
     * Adds a new history step. If the current shown step is the last in the history a new step is appended.
     * Otherwise, all newer than the current step steps are replaced with the new one. This resembles a
     * browser forward / backward navigation or an editor undo / redo function.
     */
    addHistory(step) {
      ++this.current_step
      this.steps.splice(this.current_step)
      this.steps.push(step)
    },
    /**
     * Updates the visible playground with the state of a given position in the history.
     */
    gotoHistoryPos(pos) {
      this.current_step = pos
      let step_values = []
      if (pos >= 0) {
        step_values = this.steps[pos].fields
      } else {
        step_values.length = 81
        step_values.fill(undefined)
      }
      this.list2user(step_values)
      this.calcCellOptions(this.fields)
    },
    /**
     * Goes one step back in the playground history.
     */
    undoLastStep() {
      this.gotoHistoryPos(this.current_step - 1)
    },
    /**
     * Goes one step forward in the playground history.
     */
    redoNextStep() {
      this.gotoHistoryPos(this.current_step + 1)
    },
    /**
     * Highlights the set value for a given position in the playground history.
     */
    highlightHistoryPos(pos) {
      let step = pos >= 0 ? this.steps[pos] : Step.createStep()

      for (let col = 0; col < 9; ++col) {
        for (let row = 0; row < 9; ++row) {
          let cell = Access.field(this.fields, col, row)
          cell.highlight = col == step.col && row == step.row
        }
      }
    },
    /**
     * Makes a computed next move in the playground. This is only done if the move is definitely value. E.g. only one value
     * is left in the list of possible values or an unqiue value was found for a cell.
     */
    autoChooseNextStep() {
      const step = this.autoNextStep

      if (step !== undefined) {
        this.setUserValue(step.col, step.row, step.val)
      }
    },
    /**
     * Serializes the user set values into a single list.
     */
    user2list() {
      let ret = []

      for (let col = 0; col < 9; ++col) {
        for (let row = 0; row < 9; ++row) {
          let cell = Access.field(this.fields, col, row)
          ret.push(cell.user_value)
        }
      }

      return ret
    },
    /**
     * Takes a list of user set values, as created by user2list() and sets them in the playground
     */
    list2user(l) {
      l = Array.from(l)
      for (let col = 0; col < 9; ++col) {
        for (let row = 0; row < 9; ++row) {
          let cell = Access.field(this.fields, col, row)
          cell.user_value = l.shift()
        }
      }
    }
  },
  mounted: function () {
    this.state = States.INIT
    this.clear()
  }
}
</script>

<style>
#app {
  text-align: center;
  margin-top: 10px;
}
.left {
  float: left;
  width: 70%;
}
.actions {
  margin: 20px;
  text-align: left;
}
h2.highlight {
  color: blue;
}
button {
  margin: 1em;
  padding: 1em;
}
</style>
