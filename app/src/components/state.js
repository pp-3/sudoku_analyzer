const INIT = 0
const PREDEFINED_INPUT = 1
const ANALYZE = 2

/**
 * Global constants for the different states the game may be in.
 * INIT : First start
 * PREDEFINED_INPUT : Start of the game, user enters all predefined values
 * ANALYZE : Predefined values are fix, user may enter values to solve the game
 */
export default {
  INIT: INIT,
  PREDEFINED_INPUT: PREDEFINED_INPUT,
  ANALYZE: ANALYZE
}
