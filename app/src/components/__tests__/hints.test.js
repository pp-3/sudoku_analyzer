
import { expect, test } from 'vitest'
import Hints from '../hints.js'
import Access from '../access.js'

test('cmpSets', () => {
  const s1 = new Set([1, 2, 3])
  const s2 = new Set([1, 2, 99])
  const s3 = new Set([1, 2, 3, 4])

  expect(Hints.cmpSets(s1, s1)).to.equal(true)
  expect(Hints.cmpSets(s1, s2)).to.equal(false)
  expect(Hints.cmpSets(s1, s3)).to.equal(false)
})

test('permutations', () => {
  const arr1 = [1, 2, 3]

  const p1 = Hints.permutations(arr1, 2)
  expect(p1 instanceof Array).to.equal(true)
  let solutions1 = new Set([ [1, 2].toString(), [1, 3].toString(), [2, 3].toString() ])
  expect(p1).toHaveLength(solutions1.size)
  for (const p of p1) {
    expect(p).toSatisfy((v) => {
      v = v.toString()
      const ret = solutions1.has(v)
      solutions1.delete(v)
      return ret
    })
  }

  const arr2 = [1, 2, 3, 4]

  const p2 = Hints.permutations(arr2, 2)
  let solutions2 = new Set([ [1, 2].toString(), [1, 3].toString(), [1, 4].toString(),
                             [2, 3].toString(), [2, 4].toString(),
                             [3, 4].toString() ])
  expect(p2).toHaveLength(solutions2.size)
  for (const p of p2) {
    expect(p).toSatisfy((v) => {
      v = v.toString()
      const ret = solutions2.has(v)
      solutions2.delete(v)
      return ret
    })
  }

  const p3 = Hints.permutations(arr2, 3)
  let solutions3 = new Set([ [1, 2, 3].toString(), [1, 2, 4].toString(), [1, 3, 4].toString(), [2, 3, 4].toString() ])
  expect(p3).toHaveLength(solutions3.size)
  for (const p of p3) {
    expect(p).toSatisfy((v) => {
      v = v.toString()
      const ret = solutions3.has(v)
      solutions3.delete(v)
      return ret
    })
  }

  const arr3 = [1, 2, 3, 4, 5, 6]
  const p4 = Hints.permutations(arr3, 3)
  expect(p4).toHaveLength(20) // len! / count! /  (len - count )!

  const p5 = Hints.permutations(arr3, 4)
  expect(p5).toHaveLength(15)

  const p6 = Hints.permutations(arr1, 3)
  expect(p6).toHaveLength(3)
  const p7 = Hints.permutations(arr1, 4)
  expect(p7).toHaveLength(0)
})

test('getValueDistribution', () => {
  const valid = [
    new Set([1,2,3]),
    new Set([4,5,6]),
    new Set([7,9]),
    new Set([]),
    new Set([1]),
    new Set([2,4]),
    new Set([5]),
    new Set([]),
    new Set([])
  ]

  const distribution = Hints.getValueDistribution(valid)
  expect(Hints.cmpSets(new Set(distribution.get(1)), new Set([0, 4]))).to.equal(true)
  expect(Hints.cmpSets(new Set(distribution.get(2)), new Set([0, 5]))).to.equal(true)
  expect(Hints.cmpSets(new Set(distribution.get(3)), new Set([0]))).to.equal(true)
  expect(Hints.cmpSets(new Set(distribution.get(4)), new Set([1, 5]))).to.equal(true)
  expect(Hints.cmpSets(new Set(distribution.get(5)), new Set([1, 6]))).to.equal(true)
  expect(Hints.cmpSets(new Set(distribution.get(6)), new Set([1]))).to.equal(true)
  expect(Hints.cmpSets(new Set(distribution.get(7)), new Set([2]))).to.equal(true)
  expect(Hints.cmpSets(new Set(distribution.get(8)), new Set([]))).to.equal(true)
  expect(Hints.cmpSets(new Set(distribution.get(9)), new Set([2]))).to.equal(true)
})

test('findEqualIndicesInValueDistribution', () => {
  const valid = [
    new Set([1,2,3]),
    new Set([4,5,6]),
    new Set([7,9]),
    new Set([1,3]),
    new Set([2]),
    new Set([2,7]),
    new Set([5]),
    new Set([4,6]),
    new Set([9])
  ]

  const distribution1 = Hints.getValueDistribution(valid)
  let equal1 = Hints.findEqualIndicesInValueDistribution(distribution1, 2)
  expect(equal1 instanceof Array).to.equal(true)
  let solutions1 = new Set([ [1, 3].toString(), [4, 6].toString() ])
  expect(equal1).toHaveLength(solutions1.size)
  for (const s of solutions1) {
    expect(s).toSatisfy((v) => {
      v = v.toString()
      const ret = solutions1.has(v)
      solutions1.delete(v)
      return ret
    })
  }

  valid[0] = new Set([1,3,8])
  valid[3] = new Set([1,3,8, 9])
  valid[6] = new Set([1,3,4,8])
  const distribution2 = Hints.getValueDistribution(valid)
  const equal2 = Hints.findEqualIndicesInValueDistribution(distribution2, 3)
  const solution2 = new Set([1, 3, 8])
  expect(equal2).toHaveLength(1)
  expect(Hints.cmpSets(new Set(equal2[0]), solution2)).to.equal(true)

  valid[0] = new Set([1,2,3])
  valid[7] = new Set([7,8,9])
  const distribution3 = Hints.getValueDistribution(valid)
  const equal3 = Hints.findEqualIndicesInValueDistribution(distribution3, 2)
  expect(equal3).toHaveLength(0)
})

test('removeOtherValidValuesFromCellIfContainsUniqueValues', () => {
  const valid = [
    new Set([1,3,4,5,9]),
    new Set([1,3]),
    new Set([]),
    new Set([1,4,5,6,9]),
    new Set([1,6]),
    new Set([]),
    new Set([1,3,4,6]),
    new Set([1,3,6]),
    new Set([])
  ]

  const distribution1 = Hints.getValueDistribution(valid)
  const ret1 = Hints.removeOtherValidValuesFromCellIfContainsUniqueValues(distribution1, valid, 'v', () => {
    return '  test values'
  })
  expect(ret1).to.equal(true)
  expect(Hints.cmpSets(new Set([5,9]), valid[0])).to.equal(true)
  expect(Hints.cmpSets(new Set([5,9]), valid[3])).to.equal(true)

  expect(Hints.cmpSets(new Set([1,3]), valid[1])).to.equal(true)
  expect(Hints.cmpSets(new Set([1,6]), valid[4])).to.equal(true)
  expect(Hints.cmpSets(new Set([1,3,4,6]), valid[6])).to.equal(true)

  const ret2 = Hints.removeOtherValidValuesFromCellIfContainsUniqueValues(distribution1, valid, 'v', () => {
    return '  test values'
  })
  expect(ret2).to.equal(false)
})

test('findOnlyIndicesInValueDistribution', () => {
  const valid = [
    new Set([1,2,3]),
    new Set([4,5,6]),
    new Set([1,3]),
    new Set([1,3]),
    new Set([2]),
    new Set([2,7]),
    new Set([4,5]),
    new Set([4,5,6]),
    new Set([9])
  ]

  const distribution1 = Hints.getValueDistribution(valid)
  const only1 = Hints.findOnlyIndicesInValueDistribution(distribution1, valid, 2)
  expect(only1 instanceof Array).to.equal(true)
  expect(only1).toHaveLength(1)
  expect(only1[0] instanceof Set).to.equal(true)
  const solution1 = new Set([2, 3])
  expect(Hints.cmpSets(only1[0], solution1)).to.equal(true)

  const only2 = Hints.findOnlyIndicesInValueDistribution(distribution1, valid, 3)
  expect(only2).toHaveLength(0)

  valid[2] = new Set([4,5,6])
  const distribution2 = Hints.getValueDistribution(valid)
  const only3 = Hints.findOnlyIndicesInValueDistribution(distribution2, valid, 3)
  const solution3 = new Set([1, 2, 7])
  expect(only3).toHaveLength(1)
  expect(Hints.cmpSets(only3[0], solution3)).to.equal(true)
})

test('removeOnlyValuesFromOtherCells', () => {
  const valid = [
    new Set([1,2,3]),
    new Set([4,5,6]),
    new Set([1,3]),
    new Set([1,3]),
    new Set([4,5,6]),
    new Set([2,7]),
    new Set([4,5]),
    new Set([4,5,6]),
    new Set([9])
  ]

  const distribution1 = Hints.getValueDistribution(valid)
  const ret1 = Hints.removeOnlyValuesFromOtherCells(distribution1, valid, 'v', 'distribution1')
  expect(ret1).to.equal(true)
  expect(Hints.cmpSets(new Set([2]), valid[0])).to.equal(true)
  expect(Hints.cmpSets(new Set([4,5,6]), valid[1])).to.equal(true)
  expect(Hints.cmpSets(new Set([1,3]), valid[2])).to.equal(true)
  expect(Hints.cmpSets(new Set([1,3]), valid[3])).to.equal(true)
  expect(Hints.cmpSets(new Set([4,5,6]), valid[4])).to.equal(true)
  expect(Hints.cmpSets(new Set([2,7]), valid[5])).to.equal(true)
  expect(Hints.cmpSets(new Set([]), valid[6])).to.equal(true)
  expect(Hints.cmpSets(new Set([4,5,6]), valid[7])).to.equal(true)
  expect(Hints.cmpSets(new Set([9]), valid[8])).to.equal(true)

  const ret2 = Hints.removeOnlyValuesFromOtherCells(distribution1, valid, 'v', 'distribution1')
  expect(ret2).to.equal(false)
})

test('removeValidValueFromSection', () => {
  const field = Access.newField()

  for (let col = 0; col < 9; ++col) {
    for (let row = 0; row < 9; ++row) {
      let c = Access.field(field, col, row)
      c.valid_values.add(1 + row)
    }
  }

  const val1_to_delete = 4
  const ret1 = Hints.removeValidValueFromSection(field, 3, 3, val1_to_delete, (subcol, subrow) => {
    return subcol != 0
  })
  expect(ret1).to.equal(true)
  for (let col = 0; col < 9; ++col) {
    for (let row = 0; row < 9; ++row) {
      let c = Access.field(field, col, row)
      const expected_values = col>=4 && col<=5 && row==(val1_to_delete-1) ? 0 : 1
      expect(c.valid_values.size).to.equal(expected_values)
    }
  }

  const ret2 = Hints.removeValidValueFromSection(field, 3, 3, val1_to_delete, (subcol, subrow) => {
    return subcol != 0
  })
  expect(ret2).to.equal(false)

  const ret3 = Hints.removeValidValueFromSection(field, 0, 0, val1_to_delete, (subcol, subrow) => {
    return subcol != 0
  })
  expect(ret3).to.equal(false)

  const val2_to_delete = 3
  const ret4 = Hints.removeValidValueFromSection(field, 0, 0, val2_to_delete, (subcol, subrow) => {
    return subrow == 2
  })
  expect(ret4).to.equal(true)
  for (let col = 0; col < 9; ++col) {
    for (let row = 0; row < 3; ++row) {
      let c = Access.field(field, col, row)
      const expected_values = col>=0 && col<=2 && row==(val2_to_delete-1) ? 0 : 1
      expect(c.valid_values.size).to.equal(expected_values)
    }
  }
})

test('removeValidValueFromArray', () => {
  const valid = [
    new Set([1,2,3]),
    new Set([4,5,6]),
    new Set([1,3]),
    new Set([1,3]),
    new Set([4,5,6]),
    new Set([2,7]),
    new Set([4,5]),
    new Set([4,5,6]),
    new Set([9])
  ]

  let ret = Hints.removeValidValueFromArray(valid, 8,
            (idx) => { return true },
            (idx) => { return 'remove idx: ' + idx })
  expect(ret).to.equal(false)
  ret = Hints.removeValidValueFromArray(valid, 9,
        (idx) => { return idx != 8 },
        (idx) => { return 'remove idx: ' + idx })
  expect(ret).to.equal(false)

  ret = Hints.removeValidValueFromArray(valid, 9,
        (idx) => { return idx != 1 },
        (idx) => { return 'remove idx: ' + idx })
  expect(ret).to.equal(true)
  expect(valid[8].size).to.equal(0)

  ret = Hints.removeValidValueFromArray(valid, 1,
        (idx) => { return idx != 0 },
        (idx) => { return 'remove idx: ' + idx })
  expect(ret).to.equal(true)
  expect(valid[0].size).to.equal(3)
  expect(valid[2].size).to.equal(1)
  expect(valid[3].size).to.equal(1)
  expect(valid[4].size).to.equal(3)
})
