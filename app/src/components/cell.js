export default {
  /**
   * Creates a Cell object for the playground. The current state of each cell in the (9x9) playground is determined
   * from the values in this object.
   */
  create: function () {
    return {
      /**
       * Contains the predefined flag. If set the user_value contains the predefined value.
       */
      predefined: false,
      /**
       * Contains the user chosen value. Only possible, if no predefined value is set undefined means, user hasn't set the value yet.
       */
      user_value: undefined,
      /**
       * If an user_value is set, this value depicts, if the set value breaks a game rule.
       */
      current_error: false,
      /**
       * If a cell has no value set, this set contains the values which are legal. This values are shown as hint in the playground cell. The values are calculated after each change in the playground.
       */
      valid_values: new Set(),
      /**
       * This value is calculated out of all valid_values lists in the same column, row, and section. If one is found, it means the value can only be set in this cell. In all other cells, it isn't a legal move.
       */
      unique_value: undefined,
      /**
       * Flag for the UI only, if the user requests a highlighting of this cell.
       */
      highlight: false
    }
  }
}
