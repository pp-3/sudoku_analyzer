import Access from './access.js'

/**
 * Functions relating to calculating the hints for legal values.
 */
export default {
  DEBUG: false,

  /**
   * Calculates the list of legal to set values for the given column and row and sets them in the according Cell instance.
   * @param {*} field_arrays The 2-dim playground containing the cells.
   */
  calculateValidValues: function (field_arrays, col, row) {
    let vals = new Set([1, 2, 3, 4, 5, 6, 7, 8, 9])

    // remove values from the same column and row
    this.deleteFromSet(vals, Access.getColumnValues(field_arrays, col, undefined, Access.visibleValueExtractor))
    this.deleteFromSet(vals, Access.getRowValues(field_arrays, row, undefined, Access.visibleValueExtractor))

    // return values from the same section
    this.deleteFromSet(vals, Access.getSectionValues(field_arrays, col, row, undefined, undefined, Access.visibleValueExtractor))

    let cell = Access.field(field_arrays, col, row)
    cell.valid_values = vals
  },
  removeImpossibleCombinations: function (field_arrays) {
    // remove impossible combinations of two values
    let updated = true
    while (updated) {
      updated = this.removeValidValueCombinations(field_arrays)
    }
  },
  removeValidValueCombinations: function (field_arrays) {
    let any_update = false
    if (this.DEBUG) {
      console.log('--- start search for hints ---')
    }

    for (let i = 0; i < 9; ++i) {
      // scan through sections
      const section_col = 3 * Access.sectionArrayIndexToSubIndex(i).col
      const section_row = 3 * Access.sectionArrayIndexToSubIndex(i).row
      const section = Access.getSection(field_arrays, section_col, section_row)
      const section_valid_values = section.map((cell) => cell.valid_values)
      const section_value_distribution = this.getValueDistribution(section_valid_values)

      // scan through section for 2 valid values, which appear in only 2 cells
      any_update = this.removeOtherValidValuesFromCellIfContainsUniqueValues(section_value_distribution, section_valid_values, 's', () => {
        return '  section col: ' + section_col + ', row: ' + section_row
      }) || any_update

      // check if all valid values of the same value appear in a single column / row
      for (const it of section_value_distribution) {
        const value = it[0]
        const index_arr = it[1]

        const found_in_columns = new Set()
        const found_in_rows = new Set()

        for (const idx of index_arr) {
          const sub_index = Access.sectionArrayIndexToSubIndex(idx)
          found_in_columns.add(sub_index.col + section_col)
          found_in_rows.add(sub_index.row + section_row)
        }

        if (found_in_columns.size == 1) {
          const c = found_in_columns.values().next().value
          const col = Access.getColumn(field_arrays, c)
          const col_valid_values = col.map((cell) => cell.valid_values)

          any_update = this.removeValidValueFromArray(col_valid_values, value,
                         (idx) => { return !found_in_rows.has(idx) },
                         (idx) => { return 's   remove value in single section real column: ' + c + ' / ' + idx }
                       ) || any_update
        } else if (found_in_rows.size == 1) {
          const r = found_in_rows.values().next().value
          const row = Access.getRow(field_arrays, r)
          const row_valid_values = row.map((cell) => cell.valid_values)

          any_update = this.removeValidValueFromArray(row_valid_values, value,
                         (idx) => { return !found_in_columns.has(idx) },
                         (idx) => { return 's   remove value in single section real row: ' + idx + ' / ' + r }
                       ) || any_update
        }
      } // it

      // check section for having the exact valid values in multiple cells
      any_update = this.removeOnlyValuesFromOtherCells(section_value_distribution, section_valid_values, 's', 'section col: ' + section_col + ', row: ' + section_row)
                   || any_update
    } // i

    // columns + rows
    for (let i = 0; i < 9; ++i) {
      // **************
      // *** COLUMN ***
      // **************
      const col = Access.getColumn(field_arrays, i)
      const col_valid_values = col.map((cell) => cell.valid_values)
      const col_value_distribution = this.getValueDistribution(col_valid_values)

      // scan through column for 2 valid values, which appear in only 2 cells
      any_update = this.removeOtherValidValuesFromCellIfContainsUniqueValues(col_value_distribution, col_valid_values, 'c', () => {
        return '  column: ' + i
      }) || any_update

      // check column for having the exact valid values in multiple cells
      any_update = this.removeOnlyValuesFromOtherCells(col_value_distribution, col_valid_values, 'c', 'column: ' + i)
                   || any_update

      // check, if values in column belong to the same section
      for (const [value, idxs] of col_value_distribution) {
        const section_row = new Set()
        for (const it of idxs) {
          section_row.add(Access.getSectionStartIndex(it))
        }
        if (section_row.size == 1) {
          // all rows are part of one section
          const start_r = section_row.values().next().value
          const start_c = Access.getSectionStartIndex(i)
          any_update = this.removeValidValueFromSection(field_arrays, start_c, start_r, value, (sub_col, sub_row) => {
            return (start_c + sub_col != i)
          }) || any_update
        }
      } // value

      // ***********
      // *** ROW ***
      // ***********
      const row = Access.getRow(field_arrays, i)
      const row_valid_values = row.map((cell) => cell.valid_values)
      const row_value_distribution = this.getValueDistribution(row_valid_values)

      // scan through row for 2 valid values, which appear in only 2 cells
      any_update = this.removeOtherValidValuesFromCellIfContainsUniqueValues(row_value_distribution, row_valid_values, 'r', () => {
        return '  row: ' + i
      }) || any_update

      // check row for having the exact valid values in multiple cells
      any_update = this.removeOnlyValuesFromOtherCells(row_value_distribution, row_valid_values, 'r', 'row: ' + i)
                   || any_update

      // check, if values in row belong to the same section
      for (let [value, idxs] of row_value_distribution) {
        const section_col = new Set()
        for (const it of idxs) {
          section_col.add(Access.getSectionStartIndex(it))
        }
        if (section_col.size == 1) {
          // all columns are part of one section
          const start_c = section_col.values().next().value
          const start_r = Access.getSectionStartIndex(i)
          any_update = this.removeValidValueFromSection(field_arrays, start_c, start_r, value, (sub_col, sub_row) => {
            return (start_r + sub_row != i)
          }) || any_update
        }
      } // value
    } // i

    return any_update
  },
  /**
   * Calculates the unique value for the given column and row and sets it in the according Cell instance.
   * An unique value for cell is a value which can't be set to another cell in the same column, row, or section.
   * @param {*} field_arrays The 2-dim playground containing the cells.
   */
  calcUniqueValidValue: function (field_arrays, col, row) {
    const cell = Access.field(field_arrays, col, row)
    cell.unique_value = undefined

    if (Access.visibleValueExtractor(Access.field(field_arrays, col, row)) !== undefined) {
      // already set -> no search needed
      return
    }

    if (cell.valid_values.size == 1) {
      cell.unique_value = cell.valid_values.values().next().value
      return
    }

    // search through row, column, and section for an unique value
    for (const u of cell.valid_values.values()) {
      // check row
      let found = false
      const column_vals = Access.getColumn(field_arrays, col)
      for (let i = 0; i < 9 && !found; ++i) {
        if (i != row) {
          found = column_vals[i].valid_values.has(u)
        }
      }
      if (!found) {
        cell.unique_value = u
        return
      }

      // check column
      found = false
      const row_vals = Access.getRow(field_arrays, row)
      for (let i = 0; i < 9 && !found; ++i) {
        if (i != col) {
          found = row_vals[i].valid_values.has(u)
        }
      }
      if (!found) {
        cell.unique_value = u
        return
      }

      // check section
      found = false
      const x0 = Access.getSectionStartIndex(col)
      const y0 = Access.getSectionStartIndex(row)
      const section_vals = Access.getSection(field_arrays, x0, y0)

      for (let coli = 0; coli < 3 && !found; ++coli) {
        for (let rowi = 0; rowi < 3 && !found; ++rowi) {
          const arr_index = Access.sectionSubIndexToArrayIndex(coli, rowi)
          const x = x0 + coli
          const y = y0 + rowi
          if (x != col || y != row) {
            found = section_vals[arr_index].valid_values.has(u)
          }
        }
      }

      if (!found) {
        cell.unique_value = u
        return
      }
    }
  },

  // private functions

  /**
   * Translates a set to a string
   * @param {*} aset
   */
  setToString: function (aset) {
    let ret = ''

    for (const v of aset) {
      if (ret.length > 0) {
        ret = ret + ', '
      }
      ret = ret + v
    }

    return ret
  },
  /**
   * Deletes all values from anotherSet in fromSet.
   * @param {*} fromSet The set, where all values from anotherSet get removed from.
   * @param {*} anotherSet The set to remove in fromSet
   * @return true, if fromSet changed (at least one value was removed)
   */
  deleteFromSet: function (fromSet, anotherSet) {
    const old_size = fromSet.size

    for (const v of anotherSet.values()) {
      fromSet.delete(v)
    }

    return fromSet.size != old_size
  },
  /**
   * Creates a clone from the fromSet in toSet
   * @param {*} toSet
   * @param {*} fromSet
   */
  cloneSet: function (toSet, fromSet) {
    toSet.clear()
    for (const v of fromSet) {
      toSet.add(v)
    }
  },
  /**
   * Translates an array of valid values (a set) into a map, where the keys are the possible to set values (1..9) and the
   * values is an array of indices, where this value was found
   * @param {*} array_of_valid_sets The array of sets.
   * @returns The map of values and its occurrences in the array_of_valid_sets.
   */
  getValueDistribution: function (array_of_valid_sets) {
    const ret = new Map()

    for (let i = 1; i <= 9; ++i) {
      ret.set(i, [])
    }

    for (let i = 0; i < array_of_valid_sets.length; ++i) {
      const aset = array_of_valid_sets[i]

      for (const v of aset) {
        ret.get(v).push(i)
      }
    }

    return ret
  },
  /**
   * Scans through the map of value occurrences (value mapped to list of indices) and searches for equal indices arrays.
   * @param {*} map_of_value_occurrences The map to scan through
   * @param {*} count Keep only values, where "count" of equal indices where found
   * @return An array, where each element is an array with values (keys of the map), which point to the same indices.
   */
  findEqualIndicesInValueDistribution: function (map_of_value_occurrences, count) {
    let ret = []

    const permutation_map = new Map()
    for (const it of map_of_value_occurrences) {
      const value = it[0]
      const indices = it[1]

      if (indices.length == count) {
        const permutations_in_indices = this.permutations(indices, count)
        if (permutations_in_indices.length > 0) {
          permutation_map.set(value, permutations_in_indices)
        }
      }
    }

    let keys = Array.from(permutation_map.keys())

    while (keys.length >= count) {
      const found = []

      const k = keys.shift()
      found.push(k)
      const index_list = permutation_map.get(k)
      for (const other_key of keys) {
        const other_index_list = permutation_map.get(other_key)
        if (this.cmpSets(new Set(other_index_list), new Set(index_list))) {
          found.push(other_key)
        }
      }

      if (found.length == count) {
        ret.push(found)
      }
    }

    return ret
  },
  /**
   * Removes from an array (row, column, or section) all other valid values from a cell, if certain values can only occur in that cell
   * @param {*} map_of_value_occurrences The valid value distribution map to scan through
   * @param {*} array_of_valid_sets The array of sets the map_of_value_occurrences was calculated from.
   * @param {*} origin_character A single character describing the source of the distribution
   * @param {*} origin_callback A callback returning a string describing the details of the source of the distribution
   * @return True, if at least one valid value set has been changed.
   *         False, if no change
   */
  removeOtherValidValuesFromCellIfContainsUniqueValues: function(map_of_value_occurrences, array_of_valid_sets, origin_character, origin_callback) {
    let any_update = false

    for (let VAL_COUNT = 2; VAL_COUNT <= 3; ++VAL_COUNT) {
      const value_array_of_value_lists = this.findEqualIndicesInValueDistribution(map_of_value_occurrences, VAL_COUNT)
      if (this.DEBUG && value_array_of_value_lists.length > 0) {
        console.log(origin_callback() + ', VAL_COUNT: ' + VAL_COUNT)
      }
      for (const value_list of value_array_of_value_lists) {
        const new_valid_values = new Set(value_list)

        const indices_to_update = map_of_value_occurrences.get(value_list[0])
        for (const idx of indices_to_update) {

          if (!this.cmpSets(new_valid_values, array_of_valid_sets[idx])) {
            any_update = true
            if (this.DEBUG) {
              console.log(origin_character + '   reduced valid values: ' + value_list +
                          ' in: ' + idx)
              console.log('      new: ' + this.setToString(new_valid_values))
              console.log('      old: ' + this.setToString(array_of_valid_sets[idx]))
            }
            this.cloneSet(array_of_valid_sets[idx], new_valid_values)
          }
        } // idx
      } // value_list
    } // VAL_COUNT

    return any_update
  },
  /**
   * Returns the indices in an array, where the content (available values) is equal
   * @param {*} map_of_value_occurrences The valid value distribution map to scan through
   * @param {*} array_of_valid_sets The array of sets the map_of_value_occurrences was calculated from.
   * @param {*} count Keep only values, where "count" of equal indices where found
   * @return An array of sets, where each set contains "count" indices into the array
   */
  findOnlyIndicesInValueDistribution: function (map_of_value_occurrences, array_of_valid_sets, count) {
    const ret = []

    const values_to_check = new Set([1, 2, 3, 4, 5, 6, 7, 8, 9])

    for (const it of map_of_value_occurrences) {
      // const value = it[0]
      const indices = it[1]

      for (const it2 of indices) {
        const values_to_search_for = array_of_valid_sets[it2]
        if (values_to_search_for.size != count) {
          continue
        }

        if (!values_to_check.has(it2)) {
          // value already checked
          continue
        }
        values_to_check.delete(it2)

        const equal_values_indices = new Set()
        equal_values_indices.add(it2)
        for (let i = it2 + 1; i < 9; ++i) {
          const values_to_compare_to = array_of_valid_sets[i]
          if (this.cmpSets(values_to_search_for, values_to_compare_to)) {
            equal_values_indices.add(i)
          }
        }

        if (equal_values_indices.size == count) {
          ret.push(equal_values_indices)
        }
      }
    }

    return ret
  },
  /**
   * Removes valid values from all other cells, if these values are the only indices in exact N cells, where N is the count of values.
   * @param {*} map_of_value_occurrences The valid value distribution map to scan through
   * @param {*} array_of_valid_sets The array of sets the map_of_value_occurrences was calculated from.
   * @param {*} origin_character A single character describing the source of the distribution
   * @param {*} src_index The index in the source array as a string
   * @return True, if at least one valid value set has been changed.
   *         False, if no change
   */
  removeOnlyValuesFromOtherCells: function (map_of_value_occurrences, array_of_valid_sets, origin_character, src_index) {
    let any_update = false

    for (let VAL_COUNT = 2; VAL_COUNT <= 3; ++VAL_COUNT) {
      for (const index_set of this.findOnlyIndicesInValueDistribution(map_of_value_occurrences, array_of_valid_sets, VAL_COUNT)) {
        for (let j = 0; j < 9; ++j) {
          if (index_set.has(j)) {
            continue
          }

          for (const idx of index_set) {
            const values = array_of_valid_sets[idx]
            const old = this.setToString(array_of_valid_sets[j])
            if (this.deleteFromSet(array_of_valid_sets[j], values)) {
              if (this.DEBUG) {
                console.log(
                  origin_character + '   remove only values from : ' + src_index +
                    ' at index: ' + j +
                    ', VAL_CNT: ' + VAL_COUNT +
                    ' , val: ' + this.setToString(values) +
                    ' : ' + old +
                    ' => ' + this.setToString(array_of_valid_sets[j])
                )
              }
              any_update = true
            }
          } // idx
        } // j
      } // index_set
    } // VAL_COUNT

    return any_update
  },
  /**
   * Removes from one section in the field a single valid value, if the position passes a filter
   * @param {*} field_arrays The full playground
   * @param {*} left_col The left most column index (0,3,6) of the section
   * @param {*} top_row The top most row index (0,3,6) of the section
   * @param {*} valid_value The value to remove from this one section
   * @param {*} filter_func Only cells, which pass the filter function will be changed
   *            filter_func takes two parameters: the sub column (0..2) and the sub row (0..2)
   * @return True, if at least one valid value set has been changed.
   *         False, if no change
   */
  removeValidValueFromSection: function(field_arrays, left_col, top_row, valid_value, filter_func) {
    let any_update = false

    const section = Access.getSection(field_arrays, left_col, top_row)
    const section_valid_values = section.map((cell) => cell.valid_values)

    for (let k = 0; k < 3; ++k) { // sub column
      for (let l = 0; l < 3; ++l) { // sub row
        if (!filter_func(k, l)) {
          continue
        }

        const arr_index = Access.sectionSubIndexToArrayIndex(k, l)
        if (section_valid_values[arr_index].has(valid_value)) {
          any_update = true
          const old = this.setToString(section_valid_values[arr_index])
          section_valid_values[arr_index].delete(valid_value)
          if (this.DEBUG) {
            console.log(
              '    remove values from section, which belong to single column: ' +
                (left_col + k) + ' / ' + (top_row + l) +
                ' , val: ' + valid_value +
                ' : ' + old +
                ' => ' + this.setToString(section_valid_values[arr_index])
            )
          }
        }
      } // l
    } // k

    return any_update
  },
  /**
   * Removes a valid value from the array of valid values, only if the array index fulfills a filter function.
   * @param {*} array_of_valid_sets The array of valid value sets (e.g. from one row)
   * @param {*} valid_value The value to remove from above valid value sets
   * @param {*} filter_func Only array entries, which pass the filter function will be changed
   *            filter_func takes the array index as a parameter
   * @param {*} origin_callback A callback returning a string describing the details of the source of the distribution
   *            origin_callback takes the array index as a parameter
   * @return True, if at least one valid value set has been changed.
   *         False, if no change
   */
  removeValidValueFromArray: function(array_of_valid_sets, valid_value, filter_func, origin_callback) {
    let any_update = false

    for (let j = 0; j < 9; ++j) {
      if (!filter_func(j)) {
        continue
      }
      if (array_of_valid_sets[j].has(valid_value)) {
        if (this.DEBUG) {
          console.log(origin_callback(j) +
                      ': ' + this.setToString(array_of_valid_sets[j]) + ' - ' + valid_value)
        }
        array_of_valid_sets[j].delete(valid_value)
        any_update = true
      }
    } // j

    return any_update
  },
  /**
   * Compares two sets for equality
   * @param {*} set1
   * @param {*} set2
   * @returns true, if set1 == set2
   */
  cmpSets: function (set1, set2) {
    if (set1.size != set2.size) {
      return false
    }

    for (const v of set1) {
      if (!set2.has(v)) {
        return false
      }
    }

    return true
  },
  /**
   * Returns all permutations with 'count' elements from given array
   * @param {*} arr The array to iterate over
   * @param {*} count The count of elements for one permutation
   * @return An array with permutations, where each element is an array of 'count' values
   */
  permutations: function (arr, count) {
    const ret = []

    const len = arr.length
    if (len < count) {
      return ret
    }

    if (len == count) {
      return arr
    }

    const state = []
    // initialize 'runner' through permutations
    for (let i = 0; i < count; ++i) {
      state.push(i)
    }

    // iterate until first state overflows
    while (true) {
      let idx = count - 1
      // iterate last state
      while (state[idx] < len) {
        // create the permutation list
        const permut = []
        for (const state_idx of state) {
          permut.push(arr[state_idx])
        }
        ret.push(permut)

        state[idx] = state[idx] + 1
      }

      // increase from back to front until a state overflows
      while (state[idx] > len - (count - idx) && idx > 0) {
        idx = idx - 1
        state[idx] = state[idx] + 1
      }

      // as long as first state is not overflown set start state for states right of last increased one
      if (state[0] <= len - count) {
        for (let i = idx + 1; i < count; ++i) {
          state[i] = state[i - 1] + 1
        }
      } else {
          return ret
      }
    }
  }
}
