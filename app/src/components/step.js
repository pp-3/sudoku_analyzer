/**
 * Functions for creating Step objects.
 */
export default {
  /**
   * Creates a Step object for the history list.
   * @param {*} col The column the value belongs to.
   * @param {*} row The row the value belongs to.
   * @param {*} current_value The set value in the given cell.
   * @param {*} field_state A serialized state of all fields suitable for replaying the current step.
   */
  createHistoryStep: function (col, row, current_value, field_state) {
    return {
      row: row,
      col: col,
      val: current_value,
      fields: field_state
    }
  },
  /**
   * Creates a Step object which depicts a value in a cell.
   * @param {*} col The column the value belongs to.
   * @param {*} row The row the value belongs to.
   * @param {*} value The set value in the given cell.
   */
  createStep: function (col, row, value) {
    return {
      row: row,
      col: col,
      val: value
    }
  }
}
