import Cell from './cell.js'

/**
 * Utility functions for accessing the two-dimensional array representing the play ground.
 */
export default {
  /**
   * Creates the 9x9 playground field with Cell objects.
   * @returns A new playground field
   */
  newField: function () {
    let fields = []

    for (let col = 0; col < 9; ++col) {
      let row_arr = []

      for (let row = 0; row < 9; ++row) {
        row_arr.push(Cell.create())
      }

      fields.push(row_arr)
    }

    return fields
  },
  /**
   * Returns the Cell object from the given playground field. Writing to it will change the according field_arrays cell.
   * @param {*} field_arrays The 2-dim playground to extract the Cell from.
   * @param {*} col The column.
   * @param {*} row The row.
   */
  field: function (field_arrays, col, row) {
    return field_arrays[col][row]
  },
  /**
   * Returns from a Cell object the value to show. Either:
   * - a predefined value
   * - an user provided value
   * - undefined
   * @param cell The Cell object to take the value from
   */
  visibleValueExtractor(cell) {
    return cell.user_value
  },
  /**
   * Return all Cell instances of the given row.
   * @param {*} field_arrays The 2-dim playground to extract the row from.
   * @param {*} row The row to extract.
   * @returns The row array
   */
  getRow: function (field_arrays, row) {
    let arr = []

    for (let col = 0; col < 9; ++col) {
      arr.push(this.field(field_arrays, col, row))
    }

    return arr
  },
  /**
   * Returns the already set row values of the given row in a Set.
   * If no extractor is provided, the original Cell objects from the field_arrays are returned.
   * If an extractor function is provided it's applied to the Cell objects from the field_arrays and the result is returned.
   * @param {*} field_arrays The 2-dim playground to extract the row from.
   * @param {*} row The row to extract.
   * @param {*} exclude_column An (optional) column to exclude from the returned row.
   * @param {*} extractor An (optional) function taking a Cell object. The function result is then added to the returned array.
   * @return A Set with the extracted values from the row's values, which already contain a value.
   */
  getRowValues: function (field_arrays, row, exclude_column = undefined, extractor = undefined) {
    let arr = []

    for (let col = 0; col < 9; ++col) {
      if (exclude_column !== undefined && col == exclude_column) {
        continue
      }

      const cell = this.field(field_arrays, col, row)
      if (this.visibleValueExtractor(cell) !== undefined) {
        arr.push(cell)
      }
    }

    if (extractor !== undefined) {
      arr = arr.map(extractor)
    }

    return new Set(arr)
  },
  /**
   * Return all Cell instances of the given column.
   * @param {*} field_arrays The 2-dim playground to extract the row from.
   * @param {*} col The column to extract.
   * @returns The column array
   */
  getColumn: function (field_arrays, col) {
    let arr = []

    for (let row = 0; row < 9; ++row) {
      arr.push(this.field(field_arrays, col, row))
    }

    return arr
  },
  /**
   * Returns the already set column values of the given column in a Set.
   * If no extractor is provided, the original Cell objects from the field_arrays are returned.
   * If an extractor function is provided it's applied to the Cell objects from the field_arrays and the result is returned.
   * @param {*} field_arrays The 2-dim playground to extract the column from.
   * @param {*} row The column to extract.
   * @param {*} exclude_row An (optional) row to exclude from the returned column.
   * @param {*} extractor An (optional) function taking a Cell object. The function result is then added to the returned array.
   * @return A Set with the extracted values from the column's values, which already contain a value.
   */
  getColumnValues: function (field_arrays, col, exclude_row = undefined, extractor = undefined) {
    let arr = []

    for (let row = 0; row < 9; ++row) {
      if (exclude_row !== undefined && row == exclude_row) {
        continue
      }

      const cell = this.field(field_arrays, col, row)
      if (this.visibleValueExtractor(cell) !== undefined) {
        arr.push(cell)
      }
    }

    if (extractor !== undefined) {
      arr = arr.map(extractor)
    }

    return new Set(arr)
  },
  /**
   * Return all Cell instances of the given 3x3 section.
   * @param {*} field_arrays The 2-dim playground to extract the section from.
   * @param {*} col The column to extract.
   * @param {*} col A column number crossing the requested section (e.g., 6-8 => right section, ...)
   * @param {*} row A row number crossing the requested section (e.g., 0-2 => top section, ...)
   * @returns The section array (from top left to bottom right)
   */
  getSection: function (field_arrays, col, row) {
    let arr = []

    const x0 = this.getSectionStartIndex(col)
    const y0 = this.getSectionStartIndex(row)

    for (let rowi = 0; rowi < 3; ++rowi) {
      const y = y0 + rowi

      for (let coli = 0; coli < 3; ++coli) {
        const x = x0 + coli

        arr.push(this.field(field_arrays, x, y))
      }
    }

    return arr
  },
  /**
   * getSection() returns a 3x3 section as an array. This function translates a sub index (0-2) into the according index of the returned array.
   * @param {*} subcol The column in the section.
   * @param {*} subrow The row in the section
   * @return The array index for the given sub index.
   */
  sectionSubIndexToArrayIndex: function (subcol, subrow) {
    return subcol + 3 * subrow
  },
  /**
   * getSection() returns a 3x3 section as an array. This function translates an array index into a column and sub index (0-2).
   * @param {*} array_index The array index.
   * @return An object with a "col" and "row" value.
   */
  sectionArrayIndexToSubIndex: function (array_index) {
    return {
      col: array_index % 3,
      row: parseInt(array_index / 3)
    }
  },
  /**
   * Returns the already set values in a 3x3 section in a Set.
   * If no extractor is provided, the original Cell objects from the field_arrays are returned.
   * If an extractor function is provided it's applied to the Cell objects from the field_arrays and the result is returned.
   * @param {*} field_arrays The 2-dim playground to extract the column from.
   * @param {*} col A column number crossing the requested section (e.g., 6-8 => right section, ...)
   * @param {*} row A row number crossing the requested section (e.g., 0-2 => top section, ...)
   * @param {*} exclude_column An (optional) column to exclude from the returned row.
   * @param {*} exclude_row An (optional) row to exclude from the returned column.
   * @param {*} extractor An (optional) function taking a Cell object. The function result is then added to the returned array.
   * @return A Set with the extracted values from the section values, which already contain a value.
   */
  getSectionValues: function (field_arrays, col, row, exclude_column = undefined, exclude_row = undefined, extractor = undefined) {
    let arr = []

    const x0 = this.getSectionStartIndex(col)
    const y0 = this.getSectionStartIndex(row)

    for (let coli = 0; coli < 3; ++coli) {
      const x = x0 + coli
      if (exclude_column !== undefined && x == exclude_column) {
        continue
      }

      for (let rowi = 0; rowi < 3; ++rowi) {
        const y = y0 + rowi

        if (exclude_row !== undefined && y == exclude_row) {
          continue
        }

        const cell = this.field(field_arrays, x, y)
        if (this.visibleValueExtractor(cell) !== undefined) {
          arr.push(cell)
        }
      }
    }

    if (extractor !== undefined) {
      arr = arr.map(extractor)
    }

    return new Set(arr)
  },
  /**
   * Returns the top (for a column) most or left (for a row) most index of a sections.
   * @param {*} row_or_column Any row or column index.
   * @returns The top or left most index in the same section as row_or_column points to.
   */
  getSectionStartIndex: function (row_or_column) {
    return parseInt(row_or_column / 3) * 3
  }
}
