# Sudoko Analyzer
The Sudoko Analyzer is a web application build with the VueJS 3 library ( [https://v3.vuejs.org/](https://v3.vuejs.org/) ) for analyzing
a Sudoko game step by step.

## Start
The application may be started directly from the GitLab server: [https://pp-3.gitlab.io/sudoku_analyzer/](https://pp-3.gitlab.io/sudoku_analyzer/).

## Building
First, the build environment needs to be installed:

```
cd .../sudoku_analyzer/app/
npm install
```

For building the application call:

```
cd .../sudoku_analyzer/app/
npm run build
```
